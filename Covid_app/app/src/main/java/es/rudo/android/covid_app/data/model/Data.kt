package es.rudo.android.covid_app.data.model

import java.io.Serializable

class Data(
    var _id: String,
    var casosConfirmados: Int,
    var casosUci: Int,
    var casosFallecidos: Int,
    var casosHospitalizados: Int,
    var casosRecuperados: Int,
    var casosConfirmadosDiario: Int,
    var casosUciDiario: Int,
    var casosFallecidosDiario: Int,
    var casosHospitalizadosDiario: Int,
    var casosRecuperadosDiario: Int
) {
}