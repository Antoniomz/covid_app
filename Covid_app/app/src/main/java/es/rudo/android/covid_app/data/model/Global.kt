package es.rudo.android.covid_app.data.model

import java.io.Serializable

class Global(
    var _id: String,
    var trace: Trace,
    var timeline: ArrayList<Timeline>
) {
}