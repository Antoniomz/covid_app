package es.rudo.android.covid_app.api

import es.rudo.android.covid_app.data.model.Global
import es.rudo.android.covid_app.data.model.Regiones
import es.rudo.android.covid_app.data.model.Timeline
import retrofit2.Response
import retrofit2.http.*


interface Api {

    @GET("/api/v1/es/ccaa/")
    suspend fun getComunities(): Response<Global>

    @GET("/api/v1/es/ccaa/")
    suspend fun getComunitiesData(
        @Query("fecha") date: String,
        @Query("codigo") code: String
    ): Response<Global>

}
