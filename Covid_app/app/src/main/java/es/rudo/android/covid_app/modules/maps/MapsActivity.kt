package es.rudo.android.covid_app.modules.maps

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import es.rudo.android.covid_app.MainActivity
import es.rudo.android.covid_app.R
import es.rudo.android.covid_app.data.model.Regiones
import es.rudo.android.covid_app.databinding.ActivityMapsBinding
import es.rudo.android.covid_app.modules.info.InfoActivity
import kotlinx.android.synthetic.main.activity_maps.*
import java.util.*


class MapsActivity : AppCompatActivity(),  OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var binding: ActivityMapsBinding
    private lateinit var viewModel: MapsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_maps)
        viewModel = ViewModelProvider(this).get(MapsViewModel::class.java)
        binding.lifecycleOwner = this
        binding.mapsActivity = this
        binding.mapsViewModel = viewModel

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map_frame) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        initObservers()
        setToolbar()
    }

    var gMap: GoogleMap? = null

    private fun setToolbar(){

        supportActionBar?.apply {
            title = "Casos covid-19"
        }
    }

    private fun initObservers(){

        viewModel.regiones.observe(this, Observer{
            addMarker(it)
        })

    }

    override fun onMapReady(map: GoogleMap){

        gMap = map
        val orientation = LatLng(40.0, -3.0)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(orientation, 4F))

        map.setOnMarkerClickListener(this)
    }

    var marker = MarkerOptions()

    private fun addMarker(list: ArrayList<Regiones>) {

        for (item: Regiones in list){

            val long = item.long
            val lati = item.lat
            val lat = lati?.let { long?.let { it1 -> LatLng(it, it1) } }

            marker.title(item.codigoIsoLugar)
            marker.position(lat)
            gMap?.addMarker(marker)

        }
    }

    override fun onMarkerClick(p0: Marker): Boolean {

        val bundle = Bundle()
        bundle.putString("marker", p0.title)
        val intent = Intent(this, InfoActivity::class.java)
        intent.putExtras(bundle)
        startActivity(intent)

        return false
    }
}
