package es.rudo.android.covid_app.modules.info

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.squareup.picasso.Picasso
import es.rudo.android.covid_app.R
import es.rudo.android.covid_app.databinding.ActivityInfoBinding
import es.rudo.android.covid_app.databinding.BottomDialogBinding
import java.text.SimpleDateFormat
import java.util.*


class InfoActivity : AppCompatActivity() {

    private lateinit var binding: ActivityInfoBinding
    private lateinit var bindingSheet: BottomDialogBinding
    private lateinit var viewModel: InfoViewModel

    var fecha: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_info)
        viewModel = ViewModelProvider(this).get(InfoViewModel::class.java)
        binding.lifecycleOwner = this
        binding.infoActivity = this
        binding.infoViewModel = viewModel

        setImages()
        initObservers()
        setToolbar("")
        setDataInitial()
    }

    private fun setDataInitial(){
        val id: String = intent.getStringExtra("marker").toString()
        binding.infoViewModel?.getComunitiesData("", id)
    }

    private fun setToolbar(string: String){

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.apply {
            title = ""+ string
        }
    }

    private fun initObservers(){

        viewModel.titleComunity.observe(this, androidx.lifecycle.Observer {
            setToolbar(it)
        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.toolbar_general, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId){
            R.id.action_icon_calendar -> {
                showDatePickerDialog()
            }
            android.R.id.home -> {
                onBackPressed()
            }

            R.id.action_help -> {
                showBottomSheet()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun showDatePickerDialog() {
        val datePicker =
            DatePickerFragment { day, month, year ->
                onDaySelected(
                    day,
                    month,
                    year
                )
            }
        datePicker.show(supportFragmentManager, "datePicker")
    }

    private fun onDaySelected(day: Int, month: Int, year: Int) {

        val calendar: Calendar = Calendar.getInstance()
        calendar.set(year, month, day)

        val dateFormat: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd")
        fecha = dateFormat.format(calendar.time)

        val id: String = intent.getStringExtra("marker").toString()
        binding.infoViewModel?.getComunitiesData(fecha, id)
    }

    private fun setImages(){

        val id: String = intent.getStringExtra("marker").toString()

        when(id){

            "ES-AN" -> {
                Picasso.get().load(R.drawable.andalucia).into(binding.imageComunity)
            }
            "ES-AR" -> {
                Picasso.get().load(R.drawable.aragon).into(binding.imageComunity)
            }
            "ES-AS" -> {
                Picasso.get().load(R.drawable.asturias).into(binding.imageComunity)
            }
            "ES-IB" -> {
                Picasso.get().load(R.drawable.baleares).into(binding.imageComunity)
            }
            "ES-CN" -> {
                Picasso.get().load(R.drawable.canarias).into(binding.imageComunity)
            }
            "ES-CB" -> {
                Picasso.get().load(R.drawable.cantabria).into(binding.imageComunity)
            }
            "ES-CM" -> {
                Picasso.get().load(R.drawable.mancha).into(binding.imageComunity)
            }
            "ES-CL" -> {
                Picasso.get().load(R.drawable.leon).into(binding.imageComunity)
            }
            "ES-CT" -> {
                Picasso.get().load(R.drawable.cataluna).into(binding.imageComunity)
            }
            "ES-CE" -> {
                Picasso.get().load(R.drawable.ceuta).into(binding.imageComunity)
            }
            "ES-VC" -> {
                Picasso.get().load(R.drawable.valenciana).into(binding.imageComunity)
            }
            "ES-EX" -> {
                Picasso.get().load(R.drawable.extremadura).into(binding.imageComunity)
            }
            "ES-GA" -> {
                Picasso.get().load(R.drawable.galicia).into(binding.imageComunity)
            }
            "ES-MD" -> {
                Picasso.get().load(R.drawable.madrid).into(binding.imageComunity)
            }
            "ES-ML" -> {
                Picasso.get().load(R.drawable.melilla).into(binding.imageComunity)
            }
            "ES-MC" -> {
                Picasso.get().load(R.drawable.murcia).into(binding.imageComunity)
            }
            "ES-NC" -> {
                Picasso.get().load(R.drawable.navrra).into(binding.imageComunity)
            }
            "ES-PV" -> {
                Picasso.get().load(R.drawable.vasco).into(binding.imageComunity)
            }
            "ES-RI" -> {
                Picasso.get().load(R.drawable.rioja).into(binding.imageComunity)
            }

        }
    }

    fun showBottomSheet(){

        var bottomSheetDialog = BottomSheetDialog(this)
        bindingSheet = DataBindingUtil.inflate(layoutInflater,  R.layout.bottom_dialog, null,false)
        bindingSheet.lifecycleOwner = this
        bindingSheet.infoViewModel = viewModel
        bottomSheetDialog.setContentView(bindingSheet.root)

        val id: String = intent.getStringExtra("marker").toString()
        bindingSheet.infoViewModel?.getComunitiesData(fecha, id)

        bottomSheetDialog.show()

    }
}