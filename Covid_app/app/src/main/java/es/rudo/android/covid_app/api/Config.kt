package es.rudo.android.covid_app.api

object Config {
    val API_URL = "https://covid19.secuoyas.io/api/v1/"
    val HTTP_CLIENT_AUTHORIZATION = "Bearer "
    val TYPE_ITEM_AUTHORIZATION = "Authorization"
    val TYPE_ITEM_WS_AUTH = "ws_auth"
    val GRANT_TYPE_LOGIN = "password"
    val GRANT_TYPE = "refresh_token"
    val CLIENT_ID = "34G49fpH1TIShra7Kerojennvd7tKN2WKUfEHW9Q"
    val CLIENT_SECRET =
        "1y1mcR82Cx1riWvfN9Srbd2WScXRFu0T8RzrnWejLF9q8b5uT0fBJnB6DZBaaN9QtDHy3Cgq2Qsnrnc3gSJhQ6fNgv3Qz4tty5iUToZySEL5kXIlgHQ7uH0Te5dr6ArH"
}