package es.rudo.android.covid_app.data.model

import java.io.Serializable

class Regiones(
    var nivelAdministrativo: String,
    var nombreLugar: String,
    var codigoIsoLugar: String,
    var long: Double,
    var lat: Double,
    var data: Data
) {
}