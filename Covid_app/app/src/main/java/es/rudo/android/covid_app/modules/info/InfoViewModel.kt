package es.rudo.android.covid_app.modules.info

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.covid_app.api.RetrofitClient
import es.rudo.android.covid_app.data.model.Data
import es.rudo.android.covid_app.data.model.Global
import es.rudo.android.covid_app.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class InfoViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient = RetrofitClient()

    //global cases
    val casesConfirmed = MutableLiveData<String>()
    val casesUci = MutableLiveData<String>()
    val casesDeceased = MutableLiveData<String>()
    val casesHospitalized = MutableLiveData<String>()
    val casesRecovered = MutableLiveData<String>()

    //day cases
    val dayCasesConfirmed = MutableLiveData<String>()
    val dayCasesUci = MutableLiveData<String>()
    val dayCasesDeceased = MutableLiveData<String>()
    val dayCasesHospitalized = MutableLiveData<String>()
    val dayCasesRecovered = MutableLiveData<String>()

    //title
    val titleComunity = MutableLiveData<String>()


    fun getComunitiesData(date: String, code: String) {

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getComunitiesData(date, code)
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseData = response.body() as Global
                            val info: Data = responseData.timeline[0].regiones[0].data
                            titleComunity.value = responseData.timeline[0].regiones[0].nombreLugar
                            setGlobalCases(responseData, info)
                            setDayCases(responseData, info)
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

    fun setGlobalCases(responseData: Global, info: Data){
        val info: Data = responseData.timeline[0].regiones[0].data
        casesConfirmed.value = info.casosConfirmados.toString()
        casesUci.value = info.casosUci.toString()
        casesDeceased.value = info.casosFallecidos.toString()
        casesHospitalized.value = info.casosHospitalizados.toString()
        casesRecovered.value = info.casosHospitalizadosDiario.toString()
    }

    fun setDayCases(responseData: Global, info: Data){
        val info: Data = responseData.timeline[0].regiones[0].data
        dayCasesConfirmed.value = info.casosConfirmadosDiario.toString()
        dayCasesUci.value = info.casosUciDiario.toString()
        dayCasesDeceased.value = info.casosFallecidosDiario.toString()
        dayCasesHospitalized.value = info.casosHospitalizadosDiario.toString()
        dayCasesRecovered.value = info.casosRecuperadosDiario.toString()
    }

}