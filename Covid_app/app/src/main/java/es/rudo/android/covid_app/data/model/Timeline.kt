package es.rudo.android.covid_app.data.model

import java.io.Serializable

class Timeline(
    var regiones: ArrayList<Regiones>,
    var fecha: String,
    var fechaInicio: String,
    var fechaFin: String,
    var numeroFecha: Int,
    var periodo: String
){
}