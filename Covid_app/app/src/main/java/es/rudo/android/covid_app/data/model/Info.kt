package es.rudo.android.covid_app.data.model

import java.io.Serializable

class Info(
    var _id: String,
    var nombre: String,
    var creado: String,
    var timestamp: Double,
    var fechaInicio: String,
    var fechaFin: String,
    var numeroFechas: Int,
    var periodo: String,
    var nivelAdministrativo: String,
    var nombreLugar: String,
    var codigoIsoLugar: String,
    var numeroLugares: Int,
) {
}