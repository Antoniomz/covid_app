package es.rudo.android.covid_app.modules.maps

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import es.rudo.android.covid_app.api.RetrofitClient
import es.rudo.android.covid_app.data.model.Global
import es.rudo.android.covid_app.data.model.Regiones
import es.rudo.android.covid_app.data.model.Timeline
import es.rudo.android.covid_app.helpers.Constants
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import retrofit2.Response

class MapsViewModel : ViewModel() {

    private val retrofitClient: RetrofitClient = RetrofitClient()
    val regiones = MutableLiveData<ArrayList<Regiones>>()

    init {
        getComunities()
    }


    fun getComunities() {

        CoroutineScope(Dispatchers.IO).launch {
            retrofitClient.apiCall({
                retrofitClient.getComunities()
            },
                object : RetrofitClient.RemoteEmitter {
                    override fun onResponse(response: Response<Any>) {
                        if (response.code() == Constants.SERVER_SUCCESS_CODE) {
                            val responseRegiones = response.body() as Global
                            regiones.value = responseRegiones.timeline[0].regiones
                        }
                    }

                    override fun onError(errorType: RetrofitClient.ErrorType, msg: String) {
                        Log.e("Api errortype", errorType.toString())
                        Log.e("Api message", msg)
                    }
                })
        }
    }

}